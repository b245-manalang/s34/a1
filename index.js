const express = require("express");

const port = 3000;

const app = express();

app.use(express.json());

let items = [
	    {
	        name: "Mjolnir",
	        price: 50000,
	        isActive: true
	    },
	    {
	        name: "Vibranium Shield",
	        price: 70000,
	        isActive: true
	    }

	]

		app.get("/home", (request, response) => {
			
			response.status(201).send("You are on the homepage.")
		})


		app.get("/items", (request, response) => {
			response.send(items);
		})


		app.delete("/delete-item", (request, response) => {
			
			let deletedItem = items.pop();

			response.send(deletedItem);
		})

app.listen(port, ()=> console.log("Server is running at port 3000"))